// Setting up the Pins for Arduino
const int stepPin = 3;
const int dirPin = 4;
int speedDelay;
//float rpsint;
String rps;

enum speedLevels{
  OFF,
  SLOW, //Low - 0.5 rotations per second (20)
  MEDIUM, //Medium - 1 rotation per second (10)
  FAST //High - 2 rotations per second (5)
};
enum speedLevels rpsint = OFF;
void setup () {
// Define the output pins
pinMode (stepPin, OUTPUT);
pinMode (dirPin, OUTPUT);
Serial.begin(9600);

// Setting up the direction of the Motor (Clockwise)
digitalWrite(dirPin, HIGH);
}

void loop() {
// Transforming from speed to Rotation Per Second
speedDelay = 2500/rpsint;
// Stop the Step Motor
if (rpsint == OFF){
  digitalWrite(stepPin,LOW);
}
else{
// Rotating the Step Motor
  digitalWrite(stepPin,HIGH);
  delayMicroseconds(speedDelay);
  digitalWrite(stepPin,LOW);
  delayMicroseconds(speedDelay);
}

//Serial.println(rpsint);

// Setting up the Serial Response
  // Read serial input:
  if (Serial.available() > 0) {
    int inChar = Serial.read();
    if (isDigit(inChar)) {
      // convert the incoming byte to a char and add it to the string:
      rps += (char)inChar;
      rpsint = rps.toInt();
      rps = "";
      //rpsint = inChar;
    }
    // if you get a newline, print the string, then the string's value:
    //if (inChar == '\n') {
     // rpsint = rps.toInt();
     // rps = "";
    //}
  }
}
